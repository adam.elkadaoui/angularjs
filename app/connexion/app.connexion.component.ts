import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/app.service.data';



@Component({
	
	moduleId: module.id,
	selector: 'my-connexion',
	templateUrl: 'app.connexion.html',
	styleUrls: ['app.connexion.css']
})
export class ConnexionComponent{

	titre: string = "Connexion";
	login: string;
	pwd: string;
	lblLogin : string = "Login";
	lblPassword : string = "Mot de passe";
	lblMessage: string;
	estCache: boolean = true;
	visiteur: any;

	constructor(private dataService: DataService, private router: Router){}

	valider(): void{

		this.dataService.connexion(this.login,this.pwd)
		.subscribe(
					(data)=>{ 
								this.visiteur = data;
								this.dataService.visiteur = data; 
								this.router.navigate(['accueil']);
							}
							,(error) =>{ 
								this.estCache = false;
								this.lblMessage = "erreur";
							}
					);

	}	
}
