"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_service_data_1 = require("../services/app.service.data");
var VisitesComponent = (function () {
    // compléter en ajoutant les champs présent dans le fichier HTML
    function VisitesComponent(dataService) {
        this.dataService = dataService;
        this.gestionMajRapport = false;
        this.gestionAjoutRapport = false;
        this.afficherRapport = false;
        this.qtes = [1, 2, 3, 4, 5];
        this.medicamentsSelect = new Array();
    }
    VisitesComponent.prototype.chargerMedecins = function () {
        var _this = this;
        this.dataService.chargerMedecins(this.nomMedecin)
            .subscribe(function (data) { _this.lesMedecins = data; }, function (error) { });
    };
    VisitesComponent.prototype.selectionnerMedecin = function (med) {
        this.medecin = med;
        this.lesMedecins = null; // on vide pour ne plus qu'il ne charge pas les élements
        this.nomMedecin = med.nom + " " + med.prenom + "; dep : " + med.departement;
    };
    VisitesComponent.prototype.modifierRapport = function () {
        this.gestionAjoutRapport = false;
        this.gestionMajRapport = true;
    };
    VisitesComponent.prototype.chargerVisites = function () {
        var _this = this;
        this.titre = "Médecins visité(s) ce jour:";
        this.dataService.chargerRapportsAuneDate(this.dataService.visiteur.id, this.dateVisite)
            .subscribe(function (data) {
            _this.lesRapports = data;
        });
        // écrire 2017/02/05 sur safari, ne gere pas bien les dates
    };
    VisitesComponent.prototype.selectionner = function (rapport) {
        this.rapport = rapport;
        this.afficherRapport = true;
    };
    VisitesComponent.prototype.valider = function () {
        var _this = this;
        this.dataService.majRapport(this.rapport.idRapport, this.rapport.motif, this.rapport.bilan)
            .subscribe(function (data) {
            _this.typeMessage = "alert alert-success";
            _this.messageMAJ = "Enregistrement effectué";
        }, function (error) {
            _this.typeMessage = "alert alert-danger";
            _this.messageMAJ = "Erreur lors de l'enregistrement";
        });
    };
    VisitesComponent.prototype.initNouveauRapport = function () {
        this.nomMedecin = "";
        this.bilan = "";
        this.motif = "";
        this.dateNouveauRapport = null;
        this.nomMedicament = "";
        this.qteSelect = 1;
        this.typeMessage = "";
        this.messageEnregistrement = "";
    };
    VisitesComponent.prototype.ajouterRapport = function () {
        this.initNouveauRapport();
        this.gestionAjoutRapport = true;
        this.gestionMajRapport = false;
    };
    VisitesComponent.prototype.chargerMedicaments = function () {
        var _this = this;
        this.dataService.chargerMedicaments(this.nomMedicament)
            .subscribe(function (data) { _this.lesMedicaments = data; }, function (error) { });
    };
    VisitesComponent.prototype.choisirMedicament = function (medicament) {
        this.medicamentSelect = medicament;
        this.nomMedicament = medicament.nomCommercial;
        this.lesMedicaments = null; // on vide pour ne plus qu'il ne charge pas les élements
    };
    VisitesComponent.prototype.ajouter = function () {
        this.medicamentsSelect.push({ id: this.medicamentSelect.id, nom: this.medicamentSelect.nomCommercial, qte: this.qteSelect });
        this.nomMedicament = "";
    };
    VisitesComponent.prototype.retirer = function () {
        this.medicamentsSelect.pop();
    };
    VisitesComponent.prototype.enregistrer = function () {
        var _this = this;
        this.dataService.enregistrerRapport(this.dataService.visiteur.id, this.medecin.id, this.motif, this.dateNouveauRapport, this.bilan, this.medicamentsSelect)
            .subscribe(function (data) {
            _this.typeMessage = "alert alert-success";
            _this.messageEnregistrement = "Enregistrement effectué";
        }, function (error) {
            _this.typeMessage = "alert alert-danger";
            _this.messageEnregistrement = "Erreur lors de l'enregistrement";
        });
    };
    VisitesComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-visites',
            templateUrl: 'app.visites.html'
        }),
        __metadata("design:paramtypes", [app_service_data_1.DataService])
    ], VisitesComponent);
    return VisitesComponent;
}());
exports.VisitesComponent = VisitesComponent;
//# sourceMappingURL=app.visites.component.js.map