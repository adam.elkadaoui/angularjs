import { Component } from '@angular/core';
import { DataService } from '../services/app.service.data';
import{ Observable } from 'rxjs/Rx';


@Component({
	
	moduleId: module.id,
	selector: 'my-medecins',
	templateUrl: 'app.medecins.html'
})
export class MedecinsComponent{

	lesMedecins: Array<any>;
	legende: string = "Rechercher le médecin";
	nomMedecin : string;
	estCacheMenu: boolean = true;
	medecin: any;

	afficherRapports: boolean = false;
	lesRapports: Array<any>;

	afficherMedecin: boolean = false;
	afficherMessage: boolean = false;
	lblMessage: string = "Enregistrement effectué";


	constructor( private dataService : DataService){}

	charger() : void{ 

		this.dataService.chargerMedecins(this.nomMedecin)
						.subscribe( 
									(data)=>{this.lesMedecins = data;}
									,(error)=>{} 
								); 
	}

	selectionner(monMedecin): void{

		this.medecin= monMedecin;
		this.lesMedecins = null; // on vide pour ne plus qu'il ne charge pas les élements
        this.nomMedecin = monMedecin.nom + " " + monMedecin.prenom + "; dep : " + monMedecin.departement;   
        this.legende ="";
        this.estCacheMenu = false;
        
	}

	derniersRapports(): void{

		this.afficherRapports = true;
		this.afficherMedecin = false;

        this.dataService.chargerRapports(this.medecin.id)
                        .subscribe( 
                                    (data)=>{this.lesRapports = data;}
                                    ,(error)=>{}
                                ); 

	}

	valider(): void{

		this.dataService.majMedecin(this.medecin.id, this.medecin.adresse, this.medecin.tel, this.medecin.specialitecomplementaire)
						.subscribe( 
                                    (data)=>{this.lblMessage = "Enregistrement effectué";}
                                    ,(error)=>{this.lblMessage = "Erreur";}
                                );

		this.afficherMessage = true;

	}

	majMedecin(): void{

		this.afficherRapports = false
		this.afficherMedecin = true;

	}

}
