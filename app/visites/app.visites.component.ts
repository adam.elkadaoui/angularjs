import { Component } from '@angular/core';
import { DataService} from '../services/app.service.data';
import{ Observable } from 'rxjs/Rx';

@Component({
    moduleId: module.id,
    selector: 'my-visites',
    templateUrl: 'app.visites.html'
  
})
export class VisitesComponent {

    nomMedecin : string;
    lesMedecins : Array<any>; 
    medecin : any;
    gestionMajRapport : boolean = false;
    gestionAjoutRapport : boolean = false;

    lesRapports: Array<any>;
    dateVisite: Date;
    titre: string;

    afficherRapport: boolean = false;
    rapport: any;
    typeMessage: string;
    messageMAJ: string;

    motif: string;
    bilan: string;
    dateNouveauRapport: Date;
    lesMedicaments: Array<any>;
    medicament: any;
    nomMedicament: string;
    qtes: Array<number> = [1,2,3,4,5];
    medicamentsSelect : Array<any> = new Array();
    medicamentSelect: any;
    qteSelect: number;
    messageEnregistrement: string;

    // compléter en ajoutant les champs présent dans le fichier HTML
    constructor(private dataService : DataService){

    }


    chargerMedecins(): void{

        this.dataService.chargerMedecins(this.nomMedecin)
        			.subscribe( 
        						(data)=>{this.lesMedecins = data;}
        						,(error)=>{} 
        					);
    }


    selectionnerMedecin(med) : void{

        this.medecin= med;
        this.lesMedecins = null; // on vide pour ne plus qu'il ne charge pas les élements
        this.nomMedecin = med.nom + " " + med.prenom + "; dep : " + med.departement;   
    }


    modifierRapport(): void{

        this.gestionAjoutRapport = false;
    	this.gestionMajRapport = true;
    }


    chargerVisites() : void{

        this.titre = "Médecins visité(s) ce jour:";
        this.dataService.chargerRapportsAuneDate(this.dataService.visiteur.id, this.dateVisite)
        			.subscribe( 
        						(data)=>{this.lesRapports = data;
        								}
        					);

    // écrire 2017/02/05 sur safari, ne gere pas bien les dates

    } 


    selectionner(rapport){

        this.rapport = rapport;
        this.afficherRapport = true;
    }


    valider(): void{

        this.dataService.majRapport(this.rapport.idRapport, this.rapport.motif, this.rapport.bilan)
                      .subscribe(
                                    (data)=>{
                                        this.typeMessage = "alert alert-success"; 
                                        this.messageMAJ = "Enregistrement effectué";}
                                    ,(error)=>{
                                        this.typeMessage = "alert alert-danger";
                                        this.messageMAJ = "Erreur lors de l'enregistrement";}
                                );
    }

    initNouveauRapport(){

        this.nomMedecin ="";
        this.bilan="";
        this.motif="";
        this.dateNouveauRapport = null;
        this.nomMedicament = "";
        this.qteSelect = 1;
        this.typeMessage="";
        this.messageEnregistrement ="";
    }


    ajouterRapport(): void{

    	this.initNouveauRapport();
    	this.gestionAjoutRapport = true;
    	this.gestionMajRapport = false;
    }


    chargerMedicaments(){

        this.dataService.chargerMedicaments(this.nomMedicament)
        				.subscribe( 
        							(data)=>{this.lesMedicaments = data;}
        							,(error)=>{} 
        						); 
    }


    choisirMedicament(medicament : any){

        this.medicamentSelect = medicament;
        this.nomMedicament = medicament.nomCommercial;        
        this.lesMedicaments = null; // on vide pour ne plus qu'il ne charge pas les élements
    }


    ajouter(): void{	

        this.medicamentsSelect.push({id : this.medicamentSelect.id, nom : 
        this.medicamentSelect.nomCommercial, qte : this.qteSelect});
        this.nomMedicament ="";
    }


    retirer() : void{

        this.medicamentsSelect.pop();
    }


    enregistrer(): void{

        this.dataService.enregistrerRapport(this.dataService.visiteur.id, this.medecin.id, this.motif, this.dateNouveauRapport, this.bilan, this.medicamentsSelect)
    				    .subscribe( 
                                (data)=>{
                                	this.typeMessage = "alert alert-success"; 
                                	this.messageEnregistrement = "Enregistrement effectué";}
                                ,(error)=>{
                                	this.typeMessage = "alert alert-danger";
                                	this.messageEnregistrement = "Erreur lors de l'enregistrement";}
                            );

    }

}